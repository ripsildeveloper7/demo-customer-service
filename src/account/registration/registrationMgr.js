var registrationDA = require('./registrationDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
var VendorDetail = require('../../model/vendorAccount.model');
var zeroFill = require('zero-fill');

exports.createCustomer = function (req, res) {
    try {
        registrationDA.createCustomer(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createCustomerForLandingPage = function (req, res) {
    try {
        registrationDA.createCustomerForLandingPage(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getProfileCustomer = function (req, res) {
    try {
        registrationDA.getProfileCustomer(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updateAddressDetails = function (req, res) {
    try {
        registrationDA.updateAddressDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.updateCardsDetails = function (req, res) {
    try {
        registrationDA.updateCardsDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updateProfileUpdate = function (req, res) {
    try {
        registrationDA.updateProfileUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.customerAddressUpdate = function (req, res) {
    try {
        registrationDA.customerAddressUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.customerCardDetailUpdate = function (req, res) {
    try {
        registrationDA.customerCardDetailUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteSeletedAddress = function (req, res) {
    try {
        registrationDA.deleteSeletedAddress(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteSeletedCard = function (req, res) {
    try {
        registrationDA.deleteSeletedCard(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getAllCustomer = function (req, res) {
    try {
        registrationDA.getAllCustomer(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteCustomer = function (req, res) {
    try {
        registrationDA.deleteCustomer(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.customerOverallCount = function (req, res) {
    try {
        registrationDA.customerOverallCount(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.recentRegisteredCustomer = function (req, res) {
    try {
        registrationDA.recentRegisteredCustomer(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.customerTodayCount = function (req, res) {
    try {
        registrationDA.customerTodayCount(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createCustomerByAdmin = function (req, res) {
    try {
        registrationDA.createCustomerByAdmin(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteCustomerAccount = function (req, res) {
    try {
        registrationDA.deleteCustomerAccount(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createErpVendor = function (req, res) {
    try {
        registrationDA.createErpVendor(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.createAdminAccount = function (req, res) {
    try {
        registrationDA.createAdminAccount(req, res);
    } catch (error) {
        console.log(error);
    }
}
// Vendor --------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------

exports.createVendor = function(req, res) {
    try {

        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var dateString = month+ '/' + date + '/' + year;
        var oYear = year.toString();
        var vendorYear = oYear.slice(-2);
        var vendor = "VEND";
        var locale = "en-us";
        var result = currentDate.toLocaleString(locale, {
            month: "long"
        });
        var vendorMonth = result.substr(0, 3).toUpperCase();
        VendorDetail.find({}).select().exec(function(err, data) {
            if(err) {
                res.status(500).json(err);
            } else {
                if(data.length === 0) {
                    var vendorReg = vendor + vendorYear + vendorMonth + "0001";
                    registrationDA.createVendor(req, res, dateString, vendorReg);
                } else {
                    var arrayLength = data.length - 1;
                    var maxID = data[arrayLength].vendorCode.substr(10, 4);
                    var incOrder = maxID.slice(-4);
                    var addZero = zeroFill(4, 1);
                    var result = parseInt(incOrder) + parseInt(addZero);
                    var results = zeroFill(4, result);
                    var vendorReg = vendor + vendorYear + vendorMonth + results;
                    registrationDA.createVendor(req, res, dateString, vendorReg);
                }
            }
        })
    } catch (error) {
        console.log(error);
    }
}

exports.getAllVendor = function(req, res) {
    try {
        registrationDA.getAllVendor(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getVendorByArray = function(req, res) {
    try {
        registrationDA.getVendorByArray(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteVendor = function(req, res) {
    try {
        registrationDA.deleteVendor(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorAddressUpdate = function(req, res) {
    try {
        registrationDA.vendorAddressUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteSeletedVendorAddress = function(req, res) {
    try {
        registrationDA.deleteSeletedVendorAddress(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadVendorAddressDetails = function(req, res) {
    try {
        registrationDA.uploadVendorAddressDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getVendorProfile = function(req, res) {
    try {
        registrationDA.getVendorProfile(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorOfficeAddressUpdate = function(req, res) {
    try {
        registrationDA.vendorOfficeAddressUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorSupplyAddressUpdate = function(req, res) {
    try {
        registrationDA.vendorSupplyAddressUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorPaymentUpdate = function(req, res) {
    try {
        registrationDA.vendorPaymentUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorDeliveryUpdate = function(req, res) {
    try {
        registrationDA.vendorDeliveryUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorPerformanceUpdate = function(req, res) {
    try {
        registrationDA.vendorPerformanceUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorSignatureUpdate = function(req, res) {
    try {
        registrationDA.vendorSignatureUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteSeletedVendorOfficeAddress = function(req, res) {
    try {
        registrationDA.deleteSeletedVendorOfficeAddress(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadVendorOfficeAddressDetails = function(req, res) {
    try {
        registrationDA.uploadVendorOfficeAddressDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.vendorWarehouseAddressUpdate = function(req, res) {
    try {
        registrationDA.vendorWarehouseAddressUpdate(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteSeletedVendorWarehouseAddress = function(req, res) {
    try {
        registrationDA.deleteSeletedVendorWarehouseAddress(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadVendorWarehouseAddressDetails = function(req, res) {
    try {
        registrationDA.uploadVendorWarehouseAddressDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.updateVendorBasicDetails = function(req, res) {
    try {
        registrationDA.updateVendorBasicDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updateVendorPaymentDetails = function(req, res) {
    try {
        registrationDA.updateVendorPaymentDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updateVendorOtherDetails = function(req, res) {
    try {
        registrationDA.updateVendorOtherDetails(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updateName = function(req, res) {
    try {
        registrationDA.updateName(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updateEmail = function(req, res) {
    try {
        registrationDA.updateEmail(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updateMobile = function(req, res) {
    try {
        registrationDA.updateMobile(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.ChangesPassword = function(req, res) {
    try {
        registrationDA.ChangesPassword(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.createVendorByUpload = function(req, res) {
    try {

        var currentDate = new Date();
        var date = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var dateString = month+ '/' + date + '/' + year;
        var oYear = year.toString();
        var vendorYear = oYear.slice(-2);
        var vendor = "VEND";
        var locale = "en-us";
        var result = currentDate.toLocaleString(locale, {
            month: "long"
        });
        var vendorMonth = result.substr(0, 3).toUpperCase();
        VendorDetail.find({}).select().exec(function(err, data) {
            if(err) {
                res.status(500).json(err);
            } else {
                if(data.length === 0) {
                    var vendorReg = vendor + vendorYear + vendorMonth + "0001";
                    registrationDA.createVendorByUpload(req, res, vendorReg);
                } else {
                    var arrayLength = data.length - 1;
                    var maxID = data[arrayLength].vendorCode.substr(10, 4);
                    var incOrder = maxID.slice(-4);
                    var addZero = zeroFill(4, 1);
                    var result = parseInt(incOrder) + parseInt(addZero);
                    var results = zeroFill(4, result);
                    var vendorReg = vendor + vendorYear + vendorMonth + results;
                    registrationDA.createVendorByUpload(req, res, vendorReg);
                }
            }
        })
    } catch (error) {
        console.log(error);
    }
}

exports.storeCancelChequeName = function(req, res) {
    try {
        registrationDA.storeCancelChequeName(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.storeDigitalSignatureName = function(req, res) {
    try {
        registrationDA.storeDigitalSignatureName(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updateImageCancelChequeName = function(req, res) {
    try {
        registrationDA.updateImageCancelChequeName(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updateImageDigitalSignatureName = function(req, res) {
    try {
        registrationDA.updateImageDigitalSignatureName(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.socialMediaLogin = function (req, res) {
    try {
        registrationDA.socialMediaLogin(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.resetPassword = function (req, res) {
    try {
        registrationDA.resetPassword(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.setPassword = function (req, res) {
    try {
        registrationDA.setPassword(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.changesCustomerStatus = function (req, res) {
    try {
        registrationDA.changesCustomerStatus(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getVendorCount = function (req, res) {
    try {
        registrationDA.getVendorCount(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getCustomerAddedDate = function (req, res) {
    try {
        registrationDA.getCustomerAddedDate(req, res);
    } catch (error) {
        console.log(error);
    }
}