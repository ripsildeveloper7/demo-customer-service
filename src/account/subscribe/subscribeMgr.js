var subscribeDA = require('./subscribeDA');

exports.addSubscribe = function(req, res) {
    try {
        subscribeDA.addSubscribe(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSubscribe = function(req, res) {
    try {
        subscribeDA.getSubscribe(req, res);
    } catch (error) {
        console.log(error);
    }
}