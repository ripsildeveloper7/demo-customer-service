var subscribeDetails = require('../../model/subscribe.model');

exports.addSubscribe = function(req, res) {
    subscribeDetails.find({'emailId': req.body.emailId}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new subscribeDetails();
                create.emailId = req.body.emailId;
                create.createOn = new Date();
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                res.status(200).send({'result':'Already Subscribed'});
            }
        }
    })
}
exports.getSubscribe = function(req, res) {
    subscribeDetails.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}