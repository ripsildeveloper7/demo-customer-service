var accountRoute = require('./account/accountRoute');
var settingRoute = require('./settings/settingRoute');
var inquiryRoute = require('./inquiry/inquiryRoute');
var measurementRoute = require('./measurement/measurementRoute');
var subscribeRoute =require('./Subscribe/subscribeRoute');

exports.loadRoutes = function (app) {
  accountRoute(app);
  settingRoute(app);
  inquiryRoute(app);
  measurementRoute(app);
  subscribeRoute(app);
};

