var measurementDA = require('./measurementDA');

exports.addMeasurement = function(req, res) {
    try {
        const currentDate = new Date();
        const date = currentDate.getDate();
        const month = currentDate.getMonth() + 1;
        const year = currentDate.getFullYear();
        const todayDate = month + '/' + date + '/' + year
        measurementDA.addMeasurement(req, res, todayDate); 
    } catch (error) {
        console.log(error);
    }
}
exports.getSelectedMeasurement = function(req, res) {
    try {
        measurementDA.getSelectedMeasurement(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getAllMeasurement = function(req, res) {
    try {
        measurementDA.getAllMeasurement(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getMeasurementUser = function(req, res) {
    try {
        measurementDA.getMeasurementUser(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.deleteMeasurement = function(req, res) {
    try {
        measurementDA.deleteMeasurement(req, res);
    } catch (error) {
        console.log(error);
    }
}