var Measurement = require('../model/measurement.model');

exports.addMeasurement = function(req, res, date) {
    var create = new Measurement(req.body);
    create.addedDate = date;
   /*  create.userId = req.body.userId;
    create.price = req.body.price;
    create.discount = req.body.discount;
    create.typeName = req.body.typeName;
    create.aroundBust = req.body.aroundBust;
    create.aroundAboveWaist = req.body.aroundAboveWaist;
    create.blouseLength = req.body.blouseLength;
    create.frontNeckStyle = req.body.frontNeckStyle;
    create.frontNeckDepth = req.body.frontNeckDepth;
    create.backNeckStyle = req.body.backNeckStyle;
    create.backNeckDepth = req.body.backNeckDepth;
    create.sleeveStyle = req.body.sleeveStyle;
    create.sleeveLenth = req.body.sleeveLenth;
    create.aroundArm = req.body.aroundArm;
    create.closingSide = req.body.closingSide;
    create.closingWith = req.body.closingWith;
    create.lining = req.body.lining;
    create.specialInstruction = req.body.specialInstruction;
    create.measurementName = req.body.measurementName;
    create.addedDate = date;
    create.superCategoryId = req.body.superCategoryId;
    create.mainCategoryId = req.body.mainCategoryId;
    create.subCategoryId = req.body.subCategoryId;
    create.serviceId = req.body.serviceId;
    create.typeDescription = req.body.typeDescription;
    create.productId = req.body.productId; */
    create.save(function(err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}
exports.getSelectedMeasurement = function(req, res) {
    Measurement.findOne({'_id': req.params.id}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.getAllMeasurement = function(req, res) {
    Measurement.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.getMeasurementUser = function(req, res) {
    Measurement.find({'userId': req.params.id,
                       'measurementId': req.params.mesId }).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.deleteMeasurement = function(req, res) {
    Measurement.findOneAndRemove({'_id': req.params.id}).select().exec(function(err, deleteData) {
        if (err) {
            res.status(500).json(err);
        } else {
                Measurement.find({}).select().exec(function(err, findData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(findData);
                    }
                })
        
    }
    })
}