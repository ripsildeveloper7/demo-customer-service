var measurementMgr = require('./measurementMgr');

module.exports = function(app) {
    app.route('/addmeasurementbycustomer')
        .post(measurementMgr.addMeasurement);
        app.route('/getselectedmeasurement/:id')
        .get(measurementMgr.getSelectedMeasurement);
        app.route('/getallmeasurementbyuser')
        .get(measurementMgr.getAllMeasurement);
        app.route('/getmeasurementforuser/:id/:mesId')
        .get(measurementMgr.getMeasurementUser);
        app.route('/deletemeasurement/:id')
        .delete(measurementMgr.deleteMeasurement);
}