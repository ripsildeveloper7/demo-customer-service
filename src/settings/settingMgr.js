var settingDA = require('./settingDA');

exports.getAllSetting = function(req, res) {
    try {
        settingDA.getAllSetting(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadVendorType = function(req, res) {
    try {
        settingDA.uploadVendorType(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteVendorType = function(req, res) {
    try {
        settingDA.deleteVendorType(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.uploadCompanyStatus = function(req, res) {
    try {
        settingDA.uploadCompanyStatus(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteCompanyStatus = function(req, res) {
    try {
        settingDA.deleteCompanyStatus(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.uploadCurrency = function(req, res) {
    try {
        settingDA.uploadCurrency(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteCurrency = function(req, res) {
    try {
        settingDA.deleteCurrency(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadTaxesType = function(req, res) {
    try {
        settingDA.uploadTaxesType(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteTaxesType = function(req, res) {
    try {
        settingDA.deleteTaxesType(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadModeOfPayment = function(req, res) {
    try {
        settingDA.uploadModeOfPayment(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteModeOfPayment = function(req, res) {
    try {
        settingDA.deleteModeOfPayment(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadDeliveryMode = function(req, res) {
    try {
        settingDA.uploadDeliveryMode(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteDeliveryMode = function(req, res) {
    try {
        settingDA.deleteDeliveryMode(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadReturnsOfGoods = function(req, res) {
    try {
        settingDA.uploadReturnsOfGoods(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteReturnsOfGoods = function(req, res) {
    try {
        settingDA.deleteReturnsOfGoods(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadMerchandiseDivision = function(req, res) {
    try {
        settingDA.uploadMerchandiseDivision(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteMerchandiseDivision = function(req, res) {
    try {
        settingDA.deleteMerchandiseDivision(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.uploadPaymentDays = function(req, res) {
    try {
        settingDA.uploadPaymentDays(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deletePaymentDay = function(req, res) {
    try {
        settingDA.deletePaymentDay(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.uploadTransportCostBy = function(req, res) {
    try {
        settingDA.uploadTransportCostBy(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteTransportCostBy = function(req, res) {
    try {
        settingDA.deleteTransportCostBy(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadCatalogueImageArrangeBy = function(req, res) {
    try {
        settingDA.uploadCatalogueImageArrangeBy(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteCatalogueImageArrangeBy = function(req, res) {
    try {
        settingDA.deleteCatalogueImageArrangeBy(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadChargesOfPhotoShopBy = function(req, res) {
    try {
        settingDA.uploadChargesOfPhotoShopBy(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteChargesOfPhotoShopBy = function(req, res) {
    try {
        settingDA.deleteChargesOfPhotoShopBy(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadBusinessModel = function(req, res) {
    try {
        settingDA.uploadBusinessModel(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteBusinessModel = function(req, res) {
    try {
        settingDA.deleteBusinessModel(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadDefaultDeliveryLocation = function(req, res) {
    try {
        settingDA.uploadDefaultDeliveryLocation(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deleteDefaultDeliveryLocation = function(req, res) {
    try {
        settingDA.deleteDefaultDeliveryLocation(req, res);
    } catch (error) {
        console.log(error);
    }
}