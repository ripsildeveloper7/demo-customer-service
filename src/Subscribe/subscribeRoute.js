var subscribeMgr = require('./subscribeMgr');

module.exports = function (app) {
        // Setting

    app.route('/sendNotification')               
        .post(subscribeMgr.sendPushNotification);        
    
    app.route('/pushnotificationsubscribe')            
        .post(subscribeMgr.savePushNotification);    

    /* app.route('/deletePushNotification')         
        .delete(subscribeMgr.deletePushNotification);  */  
       
}