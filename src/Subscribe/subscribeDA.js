var customerSubscription = require('../model/subscribeData.model');
const webpush = require('web-push');
const vapidKeys = {
    "publicKey": "BEe66AvTCe_qowysFNV2QsGWzgEDnUWAJq1ytVSXxtwqjcf0bnc6d5USXmZOnIu6glj1BFcj87jIR5eqF2WJFEY",
    "privateKey": "M75vu-773ly2mBZ3LdaUCzC-A8aK9p4UuNZFEawXXNo"
};
exports.sendPushNotification = function(req, res) {
    webpush.setVapidDetails(
        'mailto:example@yourdomain.org',
        vapidKeys.publicKey,
        vapidKeys.privateKey
    );
    // send the notification
    customerSubscription.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            /* res.status(200).json(findData); */
            const notificationPayload = {
                "notification": {
                    "title": 'New  Catalog Uploaded',
                    "body": '123',
                    "icon": 'https://rinteger.com/assets/images/logos/logos.png',
                    "vibrate": [100, 50, 100],
                    "data": {
                        "url": 'https://ripsiltech.com',
                        "dateOfArrival": Date.now(),
                        "primaryKey": 1
                    }
                }
            };
            Promise.all(findData.map(sub => webpush.sendNotification(
                sub.subscribeData, JSON.stringify(notificationPayload))))
            .then(() => res.status(200).json({
                findData
            }))
            .catch(err => {
                console.log(err);
                console.error("Error sending notification, reason: ", err);
                res.status(500).json(err);
            });
   
        }
    })
}

exports.savePushNotification = function(req, res) {
    var create = new customerSubscription();
    create.subscribeData = req.body;
        create.save(function(err, data) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}
