var subscribeDA = require('./subscribeDA');

exports.sendPushNotification = function(req, res) {
    try {
        subscribeDA.sendPushNotification(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.savePushNotification = function(req, res) {
    try {
        subscribeDA.savePushNotification(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deletePushNotification = function(req, res) {
    try {
        subscribeDA.deletePushNotification(req, res);
    } catch (error) {
        console.log(error);
    }
}