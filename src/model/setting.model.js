var mongoose = require('mongoose');
const vendorSchema = new mongoose.Schema({
    vendorType: [String],
    companyStatus: [String],
    currency: [String],
    taxesType: [String],
    modeOfPayment: [String],
    deliveryMode: [String],
    returnsOfGoods: [String],
    merchandiseDivision: [String],
    paymentDay: [String],
    transportCostPaidBy: [String],
    cateImgAssignBy: [String],
    chargesOfPhotoShopBy: [String],
    businessModel: [String],
    defaultDeliveyLocation: [String]
});
const Setting = mongoose.model('vendorSetting', vendorSchema);
module.exports = Setting;