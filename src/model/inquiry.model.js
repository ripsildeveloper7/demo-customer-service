var mongoose = require('mongoose');

const inquirySchema = new mongoose.Schema({
    country: String,
        emailId: String,
    customerName: String,
    description: String
});
const Inquiry = mongoose.model('customer-inquiry', inquirySchema);
module.exports = Inquiry;