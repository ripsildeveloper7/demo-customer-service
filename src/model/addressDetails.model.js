    var mongoose = require('mongoose');
    const addressFormSchema = new mongoose.Schema({
        streetAddress: String,
        building: String,
        landmark: String,
        city: String,
        state: String,
        pincode: Number,
        mobileNumber: Number,
        name: String
    });
    module.exports = addressFormSchema;