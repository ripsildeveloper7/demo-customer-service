var mongoose = require('mongoose');
const supplyLocatoinAddressFormSchema = new mongoose.Schema({
    supplyAddressLine1: String,
    supplyAddressLine2: String,
    supplyCity: String,
    supplyState: String,
    supplyPincode: String
});
module.exports = supplyLocatoinAddressFormSchema;