

var inquiryDA = require('./inquiryDA');

exports.saveInquiry = function(req, res) {
    try {
        inquiryDA.saveInquiry(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getAllInquiry = function(req, res) {
    try {
        inquiryDA.getAllInquiry(req, res);
    } catch (error) {
        console.log(error);
    }
}
