
var Inquiry = require('../model/inquiry.model');

exports.saveInquiry = function (req, res) {
    var inquiry = new Inquiry();
    inquiry.country = req.body.country,
    inquiry.emailId = req.body.emailId;
    inquiry.customerName = req.body.customerName;
    inquiry.description = req.body.description;
    inquiry.save(function (err, data) {
        if(err) {
            res.status(500).send({
                "result": 0
            });
        } else {
            res.status(200).json(data);
        }
    })
}

exports.getAllInquiry = function(req, res) {
    Inquiry.find({}).select().exec(function (err, data) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            res.status(200).json(data);
        }
    })
}