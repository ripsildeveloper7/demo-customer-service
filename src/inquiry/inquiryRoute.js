var inquiryMgr = require('./inquiryMgr');

module.exports = function (app) {
        // Setting
      app.route('/inquiry')              
        .post(inquiryMgr.saveInquiry); 

        app.route('/getallinquiry')                 // get all setting
        .get(inquiryMgr.getAllInquiry);
}